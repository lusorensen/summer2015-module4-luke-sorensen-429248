import re

#check string begins with capital letter
valid_regex= re.compile(r"^[A-Z]")

#parse out all words beginning with a capital letter (player names)
capital_regex = re.compile(r"\b[A-Z]\w*\b")

#parse out numbers for player stats
digit_regex = re.compile(r"\d")

#class with input being the lines of the txt file
class b:
    def __init__(self, name):
        self.name = name
       
    def is_valid(self):
        match = valid_regex.match(self.name)
        if match is not None:
            return True
        else:
            return False
    
    #return string of first and last name
    def player_name(self):
        match = capital_regex.findall(self.name)
        return match[0] + " " + match[1] 
    
    #return times at bat  
    def player_batted(self):
        match = digit_regex.findall(self.name)
        return match[0]

    #return number of hits
    def player_hits(self):
        match = digit_regex.findall(self.name)
        return match[1]
    
    #return number of runs
    def player_runs(self):
        match = digit_regex.findall(self.name)
        return match[2]
    
