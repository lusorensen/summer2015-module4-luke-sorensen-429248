import sys, os, re, Sort
from Sort import b

#return error if no file given
if len(sys.argv) < 2:
	sys.exit("Please enter file name")
 
filename = sys.argv[1]
 
#return error if file doesn't exist
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])

f = open(filename)

#create dictionaries
#batted = total number of times at bat
batted = {    
}

#hits= total number of hits
hits = {    
}

#avg= batting avg
avg ={
    
}


for line in f:
    
    #extract sentance from txt
    sentence = Sort.b(line.rstrip())

    #if sentece begins with a played name (not other fluff) continue
    if sentence.is_valid():
        
        #check to see if player already has key
        if (batted.has_key(sentence.player_name())):
            
            #if player has key add number of times at bat to total
            s = batted[sentence.player_name()]
            batted[sentence.player_name()]= int(s) + int(sentence.player_batted())
            
            q = hits[sentence.player_name()]
            hits[sentence.player_name()]= int(q) + int(sentence.player_hits())
                
        else:
            
            #if player not already defined game total = new total
            batted[sentence.player_name()] = int(sentence.player_batted())
            
            hits[sentence.player_name()] = int(sentence.player_hits())
                
f.close()

#iterate through at bat total
#create batting avg for each played using total hits and times at bat
for player, num in batted.items():
    avg[player]= (hits[player]/float(batted[player]))

for key, value in sorted(avg.iteritems(), reverse=True, key=lambda (k,v): (v,k)):
    print "%s: %s" % (key, "%.3f" % round(value, 3))
    
    
    

